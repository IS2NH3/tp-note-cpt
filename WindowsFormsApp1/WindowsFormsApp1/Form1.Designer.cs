﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonp = new System.Windows.Forms.Button();
            this.buttonm = new System.Windows.Forms.Button();
            this.buttonr = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonp
            // 
            this.buttonp.Location = new System.Drawing.Point(457, 131);
            this.buttonp.Name = "buttonp";
            this.buttonp.Size = new System.Drawing.Size(75, 23);
            this.buttonp.TabIndex = 0;
            this.buttonp.Text = "+";
            this.buttonp.UseVisualStyleBackColor = true;
            this.buttonp.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonm
            // 
            this.buttonm.Location = new System.Drawing.Point(228, 131);
            this.buttonm.Name = "buttonm";
            this.buttonm.Size = new System.Drawing.Size(75, 23);
            this.buttonm.TabIndex = 1;
            this.buttonm.Text = "-";
            this.buttonm.UseVisualStyleBackColor = true;
            // 
            // buttonr
            // 
            this.buttonr.Location = new System.Drawing.Point(356, 209);
            this.buttonr.Name = "buttonr";
            this.buttonr.Size = new System.Drawing.Size(75, 23);
            this.buttonr.TabIndex = 2;
            this.buttonr.Text = "RAZ";
            this.buttonr.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(368, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonr);
            this.Controls.Add(this.buttonm);
            this.Controls.Add(this.buttonp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonp;
        private System.Windows.Forms.Button buttonm;
        private System.Windows.Forms.Button buttonr;
        private System.Windows.Forms.Label label1;
    }
}

